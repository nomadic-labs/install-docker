# Docker machine configuration `config.toml`

Replacement script for option `engine-install-url` in `MachineOptions`

Replace this script https://releases.rancher.com/install-docker/19.03.9.sh

With this https://gitlab.com/nomadic-labs/install-docker/-/raw/master/dummy.sh

Docker is already installed and ready to use in our AWS AMIs.
